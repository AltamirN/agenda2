﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Agenda2.Interface
{
    interface IDAO<T>:IDisposable where T:class, new()
    {
        //metodo de inserir
        T inserir(T model);

        //metodo de atualizar
        void atualizar(T model);

        //metodo de remover
        bool remover(T model);

        //metodo de procurar um registro especifico
        T localizarPorCodigo(params Object[] keys);

        //metodo para listar
        Collection<T> ListarTudo();
    }
}
