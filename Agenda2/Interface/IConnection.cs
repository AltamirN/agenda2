﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Agenda2.Interface
{
    interface IConnection : IDisposable
    {
        //metodo para abrir a conexao
        SqlConnection Abrir();

        //Nao busca mais a conexao, uma vez dela sendo aberta
        SqlConnection Buscar();

        //fecha a conexão
        void Fechar();
    }
}
