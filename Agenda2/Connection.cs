﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Agenda2.Interface;

namespace Agenda2
{
    public class Connection : IConnection, IDisposable
    {

        private SqlConnection _connection;

        public Connection()
        {
            _connection = new SqlConnection("Data Source=DBAgenda;Initial Catalog=Agenda;Integrated Security=True");
        }


        public SqlConnection Abrir()
        {
            if (_connection.State == ConnectionState.Closed)
            {
                _connection.Open();
            }
            return _connection;
        }

        public SqlConnection Buscar()
        {
            return this.Abrir();
        }

        public void Fechar()
        {
            if (_connection.State == ConnectionState.Open)
            {
                _connection.Close();
            }
        }

        public void Dispose()
        {
            this.Fechar();
            GC.SuppressFinalize(this);

        }
    }
}
